<?php
    require_once('connection.php');

    $sql = "SELECT * FROM records";

    $result = $conn->query($sql);
    
    
   ?>
   <head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
         a,a:hover{
            color:black;
        } 
        .btn{
          
          
        }
    </style>
</head>
   <h1 align="center">Student Records</h1>
   <a href="index.php"><button  class="btn btn-info ">Register Here</button></a><br><br>
   <table  border="1" class="table table-hover" >
       <tr>
            <th>id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Contact</th>
            <th>Gender</th>
            <th colspan="2">Action</th>
            
       </tr>
       <tr>
       <?php
       
   foreach ($result as $value) {
       ?>
            <td><?php  echo $value["id"]; ?></td>
            <td><?php  echo $value["first_name"]; ?></td>
            <td><?php  echo $value["last_name"]; ?></td>
            <td><?php  echo $value["email"]; ?></td>
            <td><?php  echo "+91-".$value["contact"]; ?></td>
            <td><?php  echo $value["gender"]; ?></td>
            <td><button class="btn btn-success"><a href="edit_profile.php?id=<?php  echo $value["id"]; ?>">Edit</a></button>
            <button onclick= "return confirm('Are You Soure To delete')" class="btn btn-danger"><a href="delete_record.php?id=<?php  echo $value["id"]; ?>">Delete</a></button></td>
       </tr>
       
       <?php
      
   }

?>
<!-- <script>
    function myfun(){
        confirm("Are You Soure To delete");
    }
</script> -->